from consultas import views
from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'clinica.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
	url(r'^$', views.index, name='index'),
	url(r'^agendar/$', views.agendar, name='agendar'),
	url(r'^marcar/$', views.marcar, name='marcar'),
    url(r'^admin/', include(admin.site.urls)),
)
