$( document ).ready( function(){
	$( '.collapse' ).collapse( 'hide' );
	
	$( "input[type='radio']" ).change( function(){
		var selected = $( this );
		$( "input[type='radio']" ).each( function() {
			if( $( this ).attr( "name" ) != selected.attr( "name" ) ) {
				$( this ).prop( "checked" , false );
			}
		});
		
		$( "#dataHiddenInput" ).val( selected.data( "data" ) );
		$( "#horaHiddenInput" ).val( selected.val() );
		$( "#agendaHiddenInput" ).val( selected.data( "agenda" ) );
	} );
} );