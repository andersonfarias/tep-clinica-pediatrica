from django import forms
from django.contrib import admin
from consultas.models import Agenda, Paciente, PlanoDeSaude, Telefone

class TelefoneInlineFormset(forms.models.BaseInlineFormSet):
	def clean(self):
		count = 0
		for form in self.forms:
			try:
				if form.cleaned_data:
					count += 1
			except AttributeError:
				pass
		if count < 1:
			raise forms.ValidationError('You must have at least one telefone')


class TelefoneInline(admin.TabularInline):
	model = Telefone
	formset = TelefoneInlineFormset


class PacienteAdmin(admin.ModelAdmin):
	exclude = ('data_primeira_consulta',)
	inlines = [
		TelefoneInline,
	]


admin.site.register(Agenda)
admin.site.register(Paciente, PacienteAdmin)
admin.site.register(PlanoDeSaude)