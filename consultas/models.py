# coding=utf-8
from django.db import models

CONSULTORIO_CHOICES = (('1', 'Ilha'), ('2', 'Bonsucesso'), ('3', 'Tijuca'))
DIA_SEMANA_CHOICES = (('1', 'Segunda'), ('2', 'Terca'), ('3', 'Quarta'), ('4', 'Quinta'), ('5', 'Sexta'), ('6', 'Sábado'), ('7', 'Domingo'))

class PlanoDeSaude(models.Model):
	nome = models.CharField(max_length=80)
	limite_consultas_mes = models.IntegerField()

	def __unicode__(self):
		return self.nome

	class Meta:
		verbose_name = 'Plano de Saúde'
		verbose_name_plural = "Planos de Saúde"
		

class Paciente(models.Model):
	nome = models.CharField(max_length=80)
	endereco = models.CharField(max_length=120, null=True, blank=True)
	data_nasc = models.DateField(null=True, blank=True)
	data_primeira_consulta = models.DateField(null=True, blank=True)
	email = models.EmailField(null=True, blank=True)
	plano_de_saude = models.ForeignKey(PlanoDeSaude, null=True, blank=True)

	def __unicode__(self):
		return u'%s - %s' % (self.nome, self.email)

	class Meta:
		verbose_name = 'Paciente'
		verbose_name_plural = "Pacientes"


class Telefone(models.Model):
	numero = models.CharField(max_length=14)
	tipo = models.CharField(max_length=30)
	obs = models.CharField(max_length=120, null=True, blank=True)
	paciente = models.ForeignKey(Paciente)
	
	def __unicode__(self):
		return u'%s - %s' % (self.tipo, self.numero)

	class Meta:
		verbose_name = 'Telefone'
		verbose_name_plural = "Telefones"
		

class Agenda(models.Model):
	dia_semana = models.CharField(max_length=1, choices=DIA_SEMANA_CHOICES)
	consultorio = models.CharField(max_length=1, choices=CONSULTORIO_CHOICES)
	hora_primeira_consulta = models.TimeField()
	hora_ultima_consulta = models.TimeField()
	intervalo_consulta = models.IntegerField()
	hora_inicio_almoco = models.TimeField()
	hora_fim_almoco = models.TimeField()

	def __unicode__(self):
		return u'Consultório %s - %s' % (CONSULTORIO_CHOICES[int(self.consultorio)-1][1], DIA_SEMANA_CHOICES[int(self.dia_semana)-1][1])

	class Meta:
		verbose_name = 'Agenda'
		verbose_name_plural = "Agendas"
				
				
class Consulta(models.Model):
	data = models.DateField()
	hora = models.TimeField()
	data_limite_marcacao_futura = models.DateField()
	consulta_revisao = models.BooleanField()
	eh_encaixe = models.BooleanField(default=False)
	limite_encaixes = models.IntegerField(null=True, blank=True)
	paciente = models.ForeignKey(Paciente)
	agenda = models.ForeignKey(Agenda)

	class Meta:
		verbose_name = 'Consulta'
		verbose_name_plural = "Consultas"