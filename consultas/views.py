from django.shortcuts import render
from django.db.models import Count
from django.template import RequestContext, loader
from django.http import HttpResponse, HttpResponseRedirect
from consultas.models import Paciente, Consulta, Agenda
import datetime

def index(request):
	template = loader.get_template('index.html')
	context = RequestContext(request, {
		'pacientes': Paciente.objects.all()
	})
	return HttpResponse(template.render(context))
	
def marcar(request):
	p_id = request.POST['paciente']
	a_id = request.POST['agenda']

	consulta = Consulta()
	consulta.paciente = Paciente.objects.get(pk=int(p_id))
	consulta.agenda = Agenda.objects.get(pk=int(a_id))
	consulta.consulta_revisao = 'consulta_revisao' in request.POST
	consulta.hora = datetime.datetime.strptime( request.POST[ 'hora' ] , '%H:%M:%S' )
	consulta.data = datetime.datetime.strptime( request.POST[ 'data' ] , '%Y-%m-%d' )
	consulta.data_limite_marcacao_futura = datetime.date.today() + datetime.timedelta( days=30 )
	
	consulta.save()
	
	return HttpResponseRedirect( "/" )
	
def agendar(request):
	template = loader.get_template('agenda.html')
	p_id = request.GET['paciente']
	
	hoje = datetime.date.today()
	um_mes = datetime.timedelta(days=30)
	
	ultimas_consultas = Consulta.objects.filter(paciente__id=int(p_id)).order_by( '-data' )
	
	ultima_consulta_paciente = None
	if ultimas_consultas:
		ultima_consulta_paciente = ultimas_consultas[0]
	
	consultas = Consulta.objects.filter(data__gte=hoje).filter(data__lte=hoje + um_mes)
	agendas = Agenda.objects.all()
	
	dias_disponiveis = {}
	
	def verificar_disponibilidade( hora, data, agenda, consultas_data, dias_disponiveis):
		hora_inicio_almoco = datetime.datetime.combine( data, agenda.hora_inicio_almoco )
		hora_fim_almoco = datetime.datetime.combine( data, agenda.hora_fim_almoco )
		
		if hora >= hora_inicio_almoco and hora <= hora_fim_almoco:
			return
		
		consulta_hora = filter(lambda c: datetime.datetime.combine( data, c.hora ) == hora , consultas_data )
		if consulta_hora:
			return
			
		if not data in dias_disponiveis:
			dias_disponiveis[ data ] = {}
			dias_disponiveis[ data ][ 'horarios' ] = []
			dias_disponiveis[ data ][ 'agenda' ] = agenda
				
		dias_disponiveis[ data ]['horarios'].append( hora )	
		
	
	data_limite = hoje + datetime.timedelta(days=31)
	if ultima_consulta_paciente:
		data_limite = ultima_consulta_paciente.data_limite_marcacao_futura
	
	for i in range(0, 30):
		data = hoje + datetime.timedelta(days=i)
		
		if data > data_limite:
			break
		
		agendas_data = filter(lambda a: int(a.dia_semana) -1 == data.weekday() , agendas)
		
		if not agendas_data:
			continue
			
		consultas_data = filter(lambda c: c.data == data , consultas)
		
		for agenda in agendas_data:
			hora = datetime.datetime.combine( data, agenda.hora_primeira_consulta )
			hora_final = datetime.datetime.combine( data, agenda.hora_ultima_consulta )
			
			verificar_disponibilidade( hora, data, agenda, consultas_data, dias_disponiveis )
			while hora != hora_final:
				hora = hora + datetime.timedelta(minutes=agenda.intervalo_consulta)
				verificar_disponibilidade( hora, data, agenda, consultas_data, dias_disponiveis )
	
	context = RequestContext(request, {
		'paciente': Paciente.objects.get(pk=int(p_id)),
		'disponibilidade': dias_disponiveis
	})

	return HttpResponse(template.render(context))